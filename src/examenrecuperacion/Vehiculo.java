/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenrecuperacion;

/**
 *
 * @author delap
 */
public class Vehiculo {
    
    protected String serieDelVehiculo;
    protected String tipoDelMotor;
    protected String marca;
    protected String modelo;
    
    //Constructores

    public Vehiculo(String serieDelVehiculo, String tipoDelMotor, String marca, String modelo) {
        this.serieDelVehiculo = serieDelVehiculo;
        this.tipoDelMotor = tipoDelMotor;
        this.marca = marca;
        this.modelo = modelo;
    }
    
    public Vehiculo(){
        this.serieDelVehiculo = "";
        this.tipoDelMotor = "";
        this.marca = "";
        this.modelo = "";
    }
    
    //Get and Set

    public String getSerieDelVehiculo() {
        return serieDelVehiculo;
    }

    public void setSerieDelVehiculo(String serieDelVehiculo) {
        this.serieDelVehiculo = serieDelVehiculo;
    }

    public String getTipoDelMotor() {
        return tipoDelMotor;
    }

    public void setTipoDelMotor(String tipoDelMotor) {
        this.tipoDelMotor = tipoDelMotor;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }
    
    
    
}
