/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenrecuperacion;

/**
 *
 * @author delap
 */
public class Servicio {
    
    private int numeroDeServicio;
    private String fechaDeServicio;
    private String tipoDeServicio;
    private Vehiculo vehiculo;
    private String descipcionDelAutomovil;
    private float costoBaseDeMantenimiento;
    
    
    //Constructores

    public Servicio(int numeroDeServicio, String fechaDeServicio, String tipoDeServicio, Vehiculo vehiculo, String descipcionDelAutomovil, float costoBaseDeMantenimiento) {
        this.numeroDeServicio = numeroDeServicio;
        this.fechaDeServicio = fechaDeServicio;
        this.tipoDeServicio = tipoDeServicio;
        this.vehiculo = vehiculo;
        this.descipcionDelAutomovil = descipcionDelAutomovil;
        this.costoBaseDeMantenimiento = costoBaseDeMantenimiento;
    }
    
    public Servicio(){
        this.numeroDeServicio = 0;
        this.fechaDeServicio = "";
        this.tipoDeServicio = "";
        this.vehiculo = new Vehiculo();
        this.descipcionDelAutomovil = "";
        this.costoBaseDeMantenimiento = 0.0f;
        
    }
    
    //Get and Set

    public int getNumeroDeServicio() {
        return numeroDeServicio;
    }

    public void setNumeroDeServicio(int numeroDeServicio) {
        this.numeroDeServicio = numeroDeServicio;
    }

    public String getFechaDeServicio() {
        return fechaDeServicio;
    }

    public void setFechaDeServicio(String fechaDeServicio) {
        this.fechaDeServicio = fechaDeServicio;
    }

    public String getTipoDeServicio() {
        return tipoDeServicio;
    }

    public void setTipoDeServicio(String tipoDeServicio) {
        this.tipoDeServicio = tipoDeServicio;
    }

    public Vehiculo getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }

    public String getDescipcionDelAutomovil() {
        return descipcionDelAutomovil;
    }

    public void setDescipcionDelAutomovil(String descipcionDelAutomovil) {
        this.descipcionDelAutomovil = descipcionDelAutomovil;
    }

    public float getCostoBaseDeMantenimiento() {
        return costoBaseDeMantenimiento;
    }

    public void setCostoBaseDeMantenimiento(float costoBaseDeMantenimiento) {
        this.costoBaseDeMantenimiento = costoBaseDeMantenimiento;
    }
    public float costoDelServicio(){
        return this.getCostoBaseDeMantenimiento();
    }
    public float costoTotalDelServicio(){
        float totalPagar = 0;
        float incremento = 0;
        int tipoDeMotor = 0;
        
        if(tipoDeMotor == 1){
            incremento = this.costoDelServicio()*.25f;
            totalPagar = this.costoDelServicio() + incremento;
        }else if(tipoDeMotor == 2){
            incremento = this.costoDelServicio()*50f;
            totalPagar = this.costoDelServicio() + incremento;
        }else if(tipoDeMotor == 3){
            incremento = this.costoDelServicio()*1.50f;
            totalPagar = this.costoDelServicio() + incremento;
        }else if(tipoDeMotor == 4){
            incremento = this.costoDelServicio()*2.0f;
            totalPagar = this.costoDelServicio() + incremento;
        }
        else{
            totalPagar = this.costoDelServicio();
        }
        return totalPagar;
    }
    public float impuesto(){
        return this.costoTotalDelServicio()*.16f;
    }
}
