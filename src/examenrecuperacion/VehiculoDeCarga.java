/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenrecuperacion;

/**
 *
 * @author delap
 */
public class VehiculoDeCarga extends Vehiculo {
    
    private float numeroDeToneladasDeCarga;
    
    //Costructor

    public VehiculoDeCarga(float numeroDeToneladasDeCarga, String serieDelVehiculo, String tipoDelMotor, String marca, String modelo) {
        super(serieDelVehiculo, tipoDelMotor, marca, modelo);
        this.numeroDeToneladasDeCarga = numeroDeToneladasDeCarga;
    }

    VehiculoDeCarga() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    //Get and Set

    public float getNumeroDeToneladasDeCarga() {
        return numeroDeToneladasDeCarga;
    }

    public void setNumeroDeToneladasDeCarga(float numeroDeToneladasDeCarga) {
        this.numeroDeToneladasDeCarga = numeroDeToneladasDeCarga;
    }
    
    
    
    
}
